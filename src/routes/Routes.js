import React, { useState, useDebugValue, useEffect } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Dashboard from "../pages/Dashboard.jsx";
import Home from "../pages/Home.jsx";
import Products from "../pages/Products.jsx";
import Shoppingcart from "../pages/Shoppingcart.jsx";
import NotFound from "../pages/NotFound.jsx";
import Profile from "../pages/Profile.jsx";
import SingleProduct from "../components/SingleProduct/SingleProduct.jsx";

import * as api from '../services/apiService'
import ProductsPanel from "../pages/ProductsPanel.jsx";
import UsersPanel from "../pages/UsersPanel.jsx";
import StockPanel from "../pages/StockPanel.jsx";
import AdminPanel from "../pages/AdminPanel.jsx";

const user = JSON.parse(localStorage.getItem("user"));

export default function Routes(props) {
    return (
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/dashboard">
          {localStorage.getItem("admin") ? <Redirect to="/dashboard/products" /> : <Redirect to="/" />}
        </Route>
        <Route exact path="/dashboard/products">
          {localStorage.getItem("admin") ? <ProductsPanel /> : <Redirect to="/" />}
        </Route>
        <Route exact path="/dashboard/users">
          {localStorage.getItem("admin") ? <UsersPanel /> : <Redirect to="/" />}
        </Route>
        <Route exact path="/dashboard/stock">
          {localStorage.getItem("admin") ? <StockPanel /> : <Redirect to="/" />}
        </Route>
        <Route exact path="/dashboard/admin">
          {localStorage.getItem("admin") ? <AdminPanel /> : <Redirect to="/" />}
        </Route>
  
  
        <Route exact path="/products">
          <Products />
        </Route>
        <Route exact path="/shoppingcart">
          {!user ? <Redirect to="/" /> : <Shoppingcart />}
        </Route>
        <Route exact path="/profile">
          {user ? <Profile /> : <Redirect to="/" />}
        </Route>
        <Route exact path="/products/:productId" component={SingleProduct}>
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    );
}
