import React, { useEffect, useState } from 'react';
import './App.css';
import Routes from "./routes/Routes";
import * as api from './services/apiService'
import './App.css'

function App() {
  const [admin, setAdmin] = useState(false)
  
  useEffect(() => {
    const func = async () => {
      await api.check_admin(setAdmin)
    }
    func()
  }, [])

  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;
