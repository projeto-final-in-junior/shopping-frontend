import React from "react";
import "./Footer.css";
import TwitterLogo from "../../img/TwitterLogo.svg";
import InstagramLogo from "../../img/InstagramLogo.svg"
import FacebookLogo from "../../img/FacebookLogo.svg";

export default function Footer() {
  return (
    <div id="footerBarBG">
      <div id="internalBar">
        <div id="left_footer">
          <a
            href="https://www.facebook.com/injunioruff"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              id="footer_logo"
              alt=""
              src={FacebookLogo}
              width="30"
              height="30"
              className=""
            />
          </a>

          <a
            href="https://www.instagram.com/injunioruff/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              id="footer_logo"
              alt=""
              src={InstagramLogo}
              width="30"
              height="30"
              className=""
            />
          </a>

          <a
            href="https://www.instagram.com/injunioruff/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              id="footer_logo"
              alt=""
              src={TwitterLogo}
              width="30"
              height="30"
              className=""
            />
          </a>

          <p><span id="copyright_text">Copyright &#169; 2020 Shopp{"{IN}"}g</span></p>
        </div>
        
        <div id="right_footer">
          <p id="devby_text"><span id="devby_highlight">Desenvolvido por</span> <br />
            Bruno Rodrigues, Débora Ferreira, <br />
            Daniel Rocha e Lucas Torres
          </p>
        </div>
      </div>
    </div>
  );
}
