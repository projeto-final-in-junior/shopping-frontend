import React from 'react'
import { useState } from 'react'

import * as api from '../../services/apiService'
import { Link } from 'react-router-dom'

export default props => {
    const [product, setProduct] = useState(props.product.product)
    const [quantity, setQuantity] = useState(props.product.quantity_to_be_bought)

    const handleChange = e => {
        setQuantity(e.target.value)
        api.edit_quantity_on_cart(props.product.product.id, quantity)
    }

    const deleteProduct = () => {
        api.delete_product_from_cart(props.product.product.id)
    }
    return (
        <div className="cart-product">
            <img src="" alt=""/>

            <div className="cart-product-info">
                <h3>{product.name}teste</h3>
                <div>
                    <p>Quantidade:</p>
                    <input type="number" value={quantity} onChange={handleChange}/>
                </div>
            </div>

            <div className="cart-product-price">
                <div>
                    <Link to={"/products/" + props.product.product.id} className="btn-preset">Ver produto</Link>
                    <button className="btn-preset" onClick={deleteProduct}>Deletar produto</button>
                </div>

                <div>
                    <p>Valor unitário: <span>R$ {product.price}</span></p>
                    <p>Valor total: <span>R$ {product.price * quantity}</span></p>
                </div>
            </div>
        </div>
    )
}