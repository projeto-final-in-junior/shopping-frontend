import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import * as api from '../../services/apiService'

import CartProduct from './CartProduct'
import { Link } from 'react-router-dom'

import "./Cart.css"

export default props => {
    const [cart, setCart] = useState([])
    const [totalValue, setTotalValue] = useState(0)
    const [buyStatus, setBuyStatus] = useState(false)

    useEffect(() => {
        api.get_cart(setCart)
    }, [])
    
    const cleanCart = async () => {
        await api.clean_cart()
        window.location.reload()
    }
    
    useEffect(() => {
        let total = 0
        cart.map( product => {
            total += product.quantity_to_be_bought * product.product.price
        })
        setTotalValue(total)
        console.log(cart)
    }, [cart])

    const confirmBuy = () => {
        api.confirm_buy(setBuyStatus)
    }

    return (
        <div className="cart">
            <h2>Seu carrinho</h2>

            {cart.length > 0 ? 
                (buyStatus ?
                    <div class="cart-empty">
                        <h2>Obrigado por comprar conosco!</h2>
                        <h2>Confira seu email :)</h2>
                        <Link className="btn-preset" to="/products">Compre mais aqui</Link>
                    </div> 
                :
                    <div>
                        <div className="cart-products">
                            {cart.map( product => {
                                return <CartProduct product={product} />
                            })}
                        </div>

                        <div className="cart-total">
                        <p>Valor total: <span>R${totalValue}</span></p>
                            <div className="cart-buttons">
                                <button className="btn-preset"onClick={cleanCart}>Limpar carrinho</button>
                                <nutton className="btn-preset" onClick={confirmBuy}>Comprar agora</nutton>
                            </div>
                        </div>
                    </div>
                )
                :
                (
                <div className="cart-empty">
                    <h2>Você ainda não adicionou produtos ao carrinho</h2>
                    <Link className="btn-preset" to="/products">Compre aqui</Link>
                </div>
                )
            }
            
        </div>
    )

}