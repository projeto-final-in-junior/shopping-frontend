import React, { useState, useEffect } from 'react'
import './PanelAdmin.css'
import * as api from '../../services/apiService'

export default function PanelAdmin() {
    const [aboutTxt, setAboutTxt] = useState("");
    const [newAboutTxt, setNewAboutTxt] = useState("");

    const handleEditNewTxt = (e) => {
        setNewAboutTxt(e.target.value);
    };

    const submitNewTxt = async (e) => {
        e.preventDefault();
        await api.put_aboutus(newAboutTxt);
        window.location.reload();
    };


    useEffect(() => {
        api.get_aboutus(setAboutTxt)
      }, []);

    return (
        <div>
            <div id="panel_princ_admin">
            <h2>Administração da Loja</h2>
                <div id="store_admin">
                    <div id="resultegraf">
                        <h3 className="panel_admin_subtitle">Resultados e Gráficos</h3>
                        <div id="grafico_faturamento"></div>
                        <p className="panel_admin_text_small">Gráfico de faturamento</p>
                    </div>
                    <div id="grafvendas">
                        <p>grafico de vendas por categoria</p>
                    </div>

                </div>
                <div id="conteudo_editavel">
                    <h3 className="panel_admin_subtitle">Conteúdo editável</h3>
                    <div>
                    <h4>Sobre Nós</h4>
                    <p className="panel_admin_text_small">Atualmente</p>
                    <p>{aboutTxt}</p>
                    <p className="panel_admin_text_small">Atualizar para:</p>
                    </div>
                    <div id="newtextbtn">
                    <textarea value={newAboutTxt} onChange={handleEditNewTxt} id="panel_admin_newTxt"></textarea>
                        <div>
                    <button onClick={submitNewTxt} className="btn-preset">Atualizar</button>

                        </div>
                    </div>
                    


                </div>

            </div>
        </div>
    )
}
