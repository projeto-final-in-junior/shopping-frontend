import React, { useState }from 'react'
import { useHistory, Link } from 'react-router-dom'
import { useEffect } from 'react'

export default props => {
    const [product, setProduct] = useState(props.product)
    const [image, setImage] = useState('')

    useEffect( () => {
        setImage(product.images[0])
    }, [product])

    const getImage = () => {
        if(image) {
            const url = image.picture.url
            return url
        }
    }
    
    return (
        <div className="product">
            <img src={`https://warm-escarpment-07318.herokuapp.com${getImage()}`} alt="Foto de um produto"/>

            <h2>{product.name.length > 15 ? product.name.slice(0, 15) + "..." : product.name }</h2>
            {/* <p>prodic compras</p> */}
            <p>{product.like_amount} likes</p>
            <p>{product.category.name.length > 17 ? product.category.name.slice(0, 17) + "..." : product.category.name}</p>
            <p className="product-description">"{product.description.length > 22 ? product.description.slice(0, 22) + "..." : product.description }"</p>
            <h5>R$ {product.price.toFixed(2)}</h5>

            <Link to={`/products/${product.id}`}><button className="btn-preset">Compre aqui</button></Link>
        </div>
    )
} 