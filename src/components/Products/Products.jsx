import React, { useState, useEffect } from 'react'
import * as api from '../../services/apiService'

import Product from './Product'

import './Products.css'

export default props => {
    const [products, setProducts] = useState([])
    const [searchName, setSearchName] = useState('')
    const [category, setCategory] = useState("Categorias")
    const [categories, setCategories] = useState([])
    const [minValue, setMinValue] = useState("")
    const [maxValue, setMaxValue] = useState("")
    
    useEffect( () => {
        api.get_products(setProducts)
    }, [])

    useEffect( () => {
        api.get_categories(setCategories)
    }, [])

    const handleInputChange = e => {
        switch (e.target.name) {
            case "search_name":
                setSearchName(e.target.value)
                break
            case "category":
                setCategory(e.target.value)
                break
            case "min_value":
                setMinValue(e.target.value)
                break
            case "max_value":
                setMaxValue(e.target.value)
                break
            default:
        }
    }

    const filterProduct = product => {
        let canShow = true


        if (!product.name.toUpperCase().includes(searchName.toUpperCase()) && searchName !== "") {
            canShow = false
        } else if (category !== "Categorias" && category !== "Todos" && product.category.name !== category) {
            canShow = false
        } else if (minValue !== "" && minValue !== "0" && parseFloat(product.price) < minValue) {
            canShow = false
        } else if (maxValue !== "" && maxValue !== "0" && parseInt(product.price) > maxValue) {
            canShow = false
        }

        return canShow
    }

    return (
        <div className="products">
            <h2>Loja</h2>

            <div>
                <div className="search-bar">
                    <input type="text" name="search_name" placeholder="Nome do produto" onChange={handleInputChange}/>
        
                    <div>
                        <select name="category" id="" onChange={handleInputChange} >
                            <option disabled selected>Categorias</option>
                            <option value="Todos">Todos</option>
                            {categories.map( c => {
                                return <option value={c.name}>{c.name}</option>
                            })}
                        </select>

                        <input type="number" name="min_value" min="0" placeholder="Menor preço" onChange={handleInputChange}/>
                        <input type="number" name="max_value" min="0" placeholder="Maior preço" onChange={handleInputChange}/>
                    </div>
                </div>

                <div className="products-container">
                    {products.map( product => {
                        if (filterProduct(product)) {
                            return <Product product={product} />
                        }
                    })}
                </div>
            </div>
        </div>
    )
}