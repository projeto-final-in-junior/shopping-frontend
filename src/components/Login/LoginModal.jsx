import React from 'react'

import LoginForm from './LoginForm'
import './LoginModal.css'

export default props => {
    const toggleClass = props.canShow ? "login-modal display-block" : "login-modal display-none";

    // Retorna o componente que será renderizado na tela como um modal
    return (
        <div className={toggleClass}>
            <section className="login-modal-main">
                <div>

                    <h2>Fazer login</h2>
                    <button className="btn-preset" onClick={props.handleClose}>X</button>
                </div>
                <LoginForm />
            </section>
        </div>
    )
}