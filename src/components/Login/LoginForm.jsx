import React, { useState } from 'react'
import * as api from '../../services/apiService';

import './LoginForm.css'

export default props => {
    const [formState, setFormState] = useState({
      "user": {
        "email": "",
        "password": "",
      }
    })

    const handleLoginSubmit = async (e) => {
      e.preventDefault()

      await api.login(formState)
      window.location.reload()
    };

    const handleInputChange = (e) => {
      e.persist()
      const value = e.target.value;
      
      setFormState( () => {
        let next = formState
        next.user[e.target.name] = value
        return next
      })
    };

    return (
        <form className="login-form" onSubmit={handleLoginSubmit} onChange={handleInputChange}>
            <input type="text" name="email" placeholder="Email"/>
            <input type="password" name="password" placeholder="Senha"/>
            <input className="btn-preset" type="submit" placeholder="Logar"/>
        </form>
    )
}