import React, {useState, useEffect} from 'react'
import * as api from "../../services/apiService";

export default props => {
    const [categories, setCategories] = useState([])
    const [currentImage, setCurrentImage] = useState("")
    const [images, setImages] = useState([])
    const [newProduct, setNewProduct] = useState({
        "product":{
            "name":"",
            "price": "",
            "category_id": "",
            "description": "",
            "quantity" : ""
        }
    })
    
    useEffect(() => {
        api.get_categories(setCategories)
    }, [])

    const handleInput = e => {
        e.persist()
        
        const value = e.target.value;
        
        setNewProduct( () => {
          let next = newProduct
          next.product[e.target.name] = value
          return next
        })
    }

    const handleImageInput = e => {
        setImages(e.target.files)
    }

    const submitForm = e => {
        e.preventDefault()
        api.create_product(newProduct, images)
    }

    return (
        <form className="add-product" >
            <h2>Gerenciar Produtos</h2>

            <h3>Criar um produto</h3>

            <input type="text" name="name" placeholder="Nome do produto" onChange={handleInput}/>
            
            <select name="category_id" onChange={handleInput}>
                <option selected disabled value="">Categoria</option>
                {categories.map( c => {
                    return <option value={c.id}>{c.name}</option>
                })}
            </select>
            <textarea name="description" placeholdercols="30" rows="10" onChange={handleInput}></textarea>
            
            <div className="add-product-numbers">
                <input type="number" name="quantity" min="1" placeholder="Quantidade incial" onChange={handleInput}/>
                <input type="number" name="price" min="0" placeholder="Preço do produto" onChange={handleInput}/>
            </div>

            <div className="image-input">
                <label >Escolha uma imagem:</label>
                <input type="file" name="images" placeholder="Adicionar" onChange={handleImageInput}/>
            </div>
            <input type="submit" className="btn-preset" value="Adicionar produto" onClick={submitForm}/>
        </form>
    )
}