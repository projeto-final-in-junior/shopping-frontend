import React, { useEffect, useState } from 'react'
import * as api from "../../services/apiService";

export default props => {
    const [categories, setCategories] = useState([])
    const [newCategoryName, setNewCategotyName] = useState([])

    useEffect(() => {
        api.get_categories(setCategories)
    }, [])

    const deleteCategory = async (id) => {
        await api.delete_category(id)
        window.location.reload()
    }   

    const createCategory = async () => {
        await api.add_category(newCategoryName)
        window.location.reload()
    }

    const handleChange = e => {
        setNewCategotyName(e.target.value)
    }

    return (
        <div className="edit-categories">
            <h2>Gerenciar Categorias</h2>

            <h3>Lista de categorias</h3>
            <ul>
                {categories.map( category => {
                    return (
                        <li>
                            <h4>{category.name}</h4>
                            <div>
                                <button className="btn-preset">Editar</button>
                                <button className="btn-preset"onClick={() => deleteCategory(category.id)}>Deletar</button>
                            </div>
                        </li>
                    )
                })}
            </ul>

            <form onSubmit={createCategory}>
                <input type="text" name="categoty" placeholder="Nova categoria" onChange={handleChange}/>
                <input type="submit" className="btn-preset"value="Adicionar"/>
            </form>
        </div>
    ) 
}