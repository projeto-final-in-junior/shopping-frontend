import React, { useState, useEffect } from 'react'
import * as api from "../../services/apiService";

import EditProducts from './EditProducts';
import AddProduct from './AddProduct';
import EditCategories from './EditCategories';

import './PanelProducts.css'

export default function PanelProducts() {
   
    return (
        <div className="panel-products">
            <div className="manage-products">
                <AddProduct />
                <EditCategories />
            </div>

            <div className="manage-categories">
                <EditProducts />
            </div>

        </div>
    )
}
