import React from 'react'

import * as api from "../../services/apiService";
import { useState } from 'react';
import { useEffect } from 'react';

export default props => {
    const [products, setProducts] = useState([])

    useEffect(() => {
        api.get_products(setProducts)
    }, [])

    const deleteProduct = async (id) => {
        await api.delete_product(id)
    }
    
    return (
        <div className="edit-products">
            <h3>Lista de produtos</h3>
            <ul>
                {products.map( product => {
                    return (
                        <li>
                            <h4>{product.name}</h4>
                            <div>
                                <button className="btn-preset">Editar</button>
                                <button className="btn-preset" onClick={() => deleteProduct(product.id)}>Deletar</button>
                            </div>
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}