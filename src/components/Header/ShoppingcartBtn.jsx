import React from "react";
import { Link } from "react-router-dom";

export default function ShoppingcartBtn() {
  return (
    <div>
      <Link to="/shoppingcart" className="btn">
        Carrinho
      </Link>
    </div>
  );
}
