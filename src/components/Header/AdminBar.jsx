import React from 'react'
import { Link } from 'react-router-dom'
import { useState } from 'react'
import { useEffect } from 'react'
import { useDebugValue } from 'react'
import { get_profile } from '../../services/apiService'
export default props => {
    const user = JSON.parse(localStorage.getItem('user'));
    
    return (
        <div className="admin-bar">
            <h3>Oi, {user.user[0].name}</h3>
            <Link id="admin-link" to="/dashboard" >Dashboard</Link>
        </div>
    )
}