import React from "react";
import { Link } from "react-router-dom";

export default function ProductsBtn() {
  return (
    <div>
      <Link to="/products" className="btn">
        Produtos
      </Link>
    </div>
  );
}
