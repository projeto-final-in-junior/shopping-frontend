import React from "react";
import { Link, useHistory } from "react-router-dom";

export default function LogoutBtn() {
  let history = useHistory();
  
  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("admin");
    history.push("/");
    window.location.reload()
  };

  return (
    <div>
      <Link onClick={logout} className="btn">
        Sair
      </Link>
    </div>
  );
}
