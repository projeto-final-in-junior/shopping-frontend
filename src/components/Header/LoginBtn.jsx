import React, { useState } from "react";
import { Link } from "react-router-dom";

import LoginModal from '../Login/LoginModal'

export default function LoginBtn(props) {
  const [canShow, setCanShow] = useState(false)

  const showLogin = () => setCanShow(true)
  const hideLogin = () => setCanShow(false)

  return (
    <div>
      <Link onClick={showLogin} className="btn">
        Login
      </Link>
      
      <LoginModal canShow={canShow} handleClose={hideLogin}/>
    </div>
  );
}
