import React, { useState } from "react";
import { Link } from "react-router-dom";

import RegisterModal from '../Register/RegisterModal'

export default function RegisterBtn() {
  const [canShow, setCanShow] = useState(false)

  const showRegister = () => setCanShow(true)
  const hideRegister = () => setCanShow(false)

  return (
    <div>
      <Link onClick={showRegister} className="btn">
        Cadastro
      </Link>

      <RegisterModal canShow={canShow} handleClose={hideRegister}/>
    </div>
  );
}
