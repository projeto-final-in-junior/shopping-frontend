import React from "react";
import { Link } from "react-router-dom";

export default function ProfileBtn() {
  return (
    <div>
      <Link to="/profile" className="btn">
        Perfil
      </Link>
    </div>
  );
}
