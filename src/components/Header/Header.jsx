import React from "react";
import "./Header.css";
import wolfLogo from "../../img/wolfLogo.svg";
import ShoppingcartBtn from "./ShoppingcartBtn.jsx";
import ProductsBtn from "./ProductsBtn.jsx";
import ProfileBtn from "./ProfileBtn.jsx";
import LogoutBtn from "./LogoutBtn.jsx";
import LoginBtn from "./LoginBtn.jsx";
import RegisterBtn from "./RegisterBtn.jsx";
import { Link } from "react-router-dom";
import AdminBar from "./AdminBar";

import * as api from '../../services/apiService'
import { useState } from "react";
import { useEffect } from "react";

export default function Header() {
  const user = JSON.parse(localStorage.getItem('user'));
  const [isAdmin, setIsAdmin] = useState(false)

  useEffect(() => {
    api.check_admin(setIsAdmin)
  }, [])

  return (
    <div className="sticky-wrapper" id="headerBarBG">
    {isAdmin ? <AdminBar /> : null}
      <div className="sticky-inner" id="internalHeaderBar">
        <div id="header_leftdiv">
          <div>
          <Link to="/">
              <img
                id="header_logo"
                alt=""
                src={wolfLogo}
                width="60"
                height="60"
                />
              </Link>
          </div>
        </div>
        <div id="header_rightdiv">
          {user && <ShoppingcartBtn />}
          <ProductsBtn />
          {user && <ProfileBtn />}
          {user && <LogoutBtn />}
          {!user && <LoginBtn />}
          {!user && <RegisterBtn />}
        </div>
      </div>
    </div>
  );
}
