import React, { useState } from 'react'
import * as api from '../../services/apiService'

export default props => {
    const [commentContent, setCommentContent] = useState('')

    const handleSubmit = async event => {
        event.preventDefault()
        await api.post_comment(props.product.id, commentContent)
        // window.location.reload()
    }

    const handleInputChange = event => {
        setCommentContent(event.target.value)
    }

    return (
        <form className="comment_form">
            <h3>Avalie este produto:</h3>

            <div>
                <textarea name="commentContent" rows="5" onChange={handleInputChange} value={commentContent} placeholder="Avalie este produto..."></textarea>
                <input className="btn-preset" type="submit" value="Enviar" onClick={handleSubmit}/>
            </div>
        </form>
    )
}