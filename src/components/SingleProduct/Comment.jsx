import React, { useEffect, useState } from 'react'
import * as api from '../../services/apiService'

export default props => {
    const [likes, setLikes] = useState(0)
    const [liked, setLiked] = useState(false)

    const updateLikes = () => {
        setLikes(likes + 1)
    }

    const decLikes = () => {
        setLikes(likes - 1)
    }
    const handleLike = () => {
        if (!liked) {
            api.like_comment(props.comment.id, setLiked, updateLikes)
        } else {
            api.unlike_comment(props.comment.id, setLiked, decLikes)
        }
    }

    useEffect(() => {
        setLikes(props.comment.likes)
    }, [props])

    useEffect(() => {
        if (props.likedList.includes(props.comment.id)) {
            setLiked(true)
        }
    }, [props.likedList, props.comment.id])
    
    return (
        <div className="comment">
            <div >
                <h2>{props.comment.name}</h2>
                <div className="comment_likes">
                    <div className="comment_like_img" onClick={handleLike}>
                        {liked ? 
                            <img src={require('../../img/starFilled.svg')} alt=""/>
                            :
                            <img src={require('../../img/star.svg')} alt=""/>
                        }
                    </div>
                    <h2>{likes}</h2>
                </div>
            </div>

            <p>{props.comment.content}</p>
        </div>
    )
}