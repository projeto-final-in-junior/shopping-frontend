import React, { useState, useEffect} from 'react'
import { useHistory } from 'react-router-dom'
import * as api from '../../services/apiService'

export default props => {
    const [quantity, setQuantity] = useState(1)
    const [images, setImages] = useState({})
    const [likes, setLikes] = useState(0)
    const [liked, setLiked] = useState(false)
    const [image, setImage] = useState('')
    const history = useHistory()

    const handleQuantity = e => {
        setQuantity(e.target.value)
    }

    const incLikes = () => {
        setLikes(likes + 1)
    }

    const decLikes = () => {
        setLikes(likes - 1)
    }

    const handleLike = async () => {
        if (!liked) {
            await api.like_product(props.product.id, setLiked, incLikes) 
        } else {
            await api.unlike_product(props.product.id, setLiked, decLikes)
        }
    }
    
    const getImage = () => {
        if(image) {
            const url = image.picture.url
            return url
        }
    }

    const addToCart = () => {
        api.add_to_cart(props.product.id, quantity)
    }

    const buyNow = async () => {
        await api.add_to_cart(props.product.id, quantity)
        history.push("/shoppingcart")
    }

    useEffect( () => {
        if (images) {
            setImage(images[0])
        }
    }, [props])

    useEffect(() => {
        api.check_product_like(props.product.id, setLiked)
    }, [props.product.id])

    useEffect(() => {
        setLikes(props.product.like_amount)
    }, [props])

    useEffect(() => {
        setImages(props.product.images)
    }, [props])

    return (
        <div id="product_show">
            <div id="product_image" >
                <img src={`https://warm-escarpment-07318.herokuapp.com${getImage()}`} alt=""/>
            </div>
            
            <div id="aside_panel">
                <div id="upper_panel">
                    <div id="name_panel">
                        <h2 className="product_name">{props.product.name}</h2>
                        <p className="prodcut_category">{props.productCategory.name}</p>
                        <div className="product_likes">
                            <div className="product_like_img" onClick={handleLike}>
                                {liked ? 
                                    <img src={require('../../img/starFilled.svg')} alt=""/>
                                    :
                                    <img src={require('../../img/star.svg')} alt=""/>
                                }
                            </div>
                            <p>{likes} likes</p>
                        </div>
                        
                        <div id="product_quantity">
                            <p>Quantidade:</p>
                            <input type="range" min="1" max={props.product.quantity} onChange={handleQuantity} value={quantity}/>
                            <div>
                                <p className="current_quantity">{quantity}</p>
                                <p className="max_quantity">{props.product.quantity}</p>
                            </div>
                        </div>
                    </div>

                    <div id="price_and_buttons">
                        <div id="product_price">
                            <p>R$ {props.product.price}</p>
                        </div>

                        <div id="product_buttons">
                            <button class="btn_single btn-preset" onClick={addToCart}>Adicionar ao carrinho</button>
                            <button class="btn_single btn-preset" onClick={buyNow}>Comprar agora</button>
                        </div>
                    </div>

                </div>

                <div id="txt_descr">
                    <p>{props.product.description}</p>
                </div>
            </div>
        </div>
    )
}