import React, { useEffect, useState } from 'react'
import * as api from '../../services/apiService'

import Comment from './Comment'
import CommentForm from './CommentForm'

import './Comments.css'

export default props => {
    const [likedList, setLikedList] = useState([])

    useEffect(() => {
        api.check_comment_like(props.product.id, setLikedList)
    }, [props.product.id])

    return (
        <div className="product_comments">

            <CommentForm product={props.product} />
            
            {props.productComments.length !== 0 ? 
                <h2>Avaliações do produto:</h2> 
                :   
                <h2>Este produto ainda não tem avaliações.</h2> 
            }

            {/* rederiza os comentários */}
            {props.productComments.map( comment => {
                return <Comment comment={comment} likedList={likedList}set/>
            })}
        </div>
    )
}