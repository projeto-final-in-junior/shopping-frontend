import React, { useEffect, useState } from 'react'
import * as api from '../../services/apiService'

import './SingleProduct.css'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'
import ProductShow from './ProductShow'
import Comments from './Comments'

export default props => {
    const [product, setProduct] = useState({})
    const [productCategory, setProductCategory] = useState({})
    const [productComments, setProductComments] = useState([])

    useEffect( () => {
        const productId= props.match.params.productId
        api.get_single_product(productId, setProduct, setProductCategory, setProductComments)
    }, [props.match.params.productId])

    return (
        
        <div className="single_product_container">
            <Header />

            <div className="single_product">
                <ProductShow product={product} productCategory={productCategory}/>
                <Comments product={product} productComments={productComments}/>
            </div>

            <Footer />
        </div>
    )
}
