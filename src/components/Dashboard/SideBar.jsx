import React, { useState } from "react";
import "./SideBar.css";
import PanelProducts from "../PanelProducts/PanelProducts";
import PanelUsers from "../PanelUsers/PanelUsers";
import PanelStock from "../PanelStock/PanelStock";
import PanelAdmin from "../PanelAdmin/PanelAdmin";
import { Link } from "react-router-dom";

export default function SideBar(props) {
  const [panelId, setPanelId] = useState("panelProducts");

  return (
    <div>
      <div id="fixSideBar">
        <div id="SideBarlinks">
            <p className="linkParagraph"><Link to="/dashboard/products" >Produtos e Categorias</Link></p>
            <p className="linkParagraph"><Link to="/dashboard/users" >Usuários</Link></p>
            <p className="linkParagraph"><Link to="/dashboard/stock" >Estoque</Link></p>
            <p className="linkParagraph"><Link to="/dashboard/admin" >Administração</Link></p>
        </div>
       {props.children}
      </div>
    </div>
  );
}
