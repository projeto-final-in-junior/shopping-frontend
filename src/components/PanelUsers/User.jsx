import React from 'react'
import * as api from "../../services/apiService";
import { useHistory } from 'react-router-dom'

export default function User(props) {

    const history = useHistory()

    const handleChangeUserPrivileges = async (e) => {
        e.preventDefault();
        await api.changeUserPrivileges(props.user.id);
        window.location.reload();
    };
    
    const handleDeleteUser = async (e) => {
        e.preventDefault();
        await api.delete_user(props.user.id);
        window.location.reload();
}
    return (
        <div id="user_card">
            <p className="user_card_item">{props.user.name}</p>
            <p className="user_card_item">{props.user.email}</p>
            <p className="user_card_item">XXX.XXX.XXX-XX</p>


            {props.user.kind === "user" ? <button className="user_card_item btn-preset" onClick={handleChangeUserPrivileges}>Tornar Admin</button> : <button onClick={handleChangeUserPrivileges} className="user_card_item btn-preset-alt">Tornar User</button> }
            <button className="user_card_item btn-preset" onClick={handleDeleteUser}>Remover</button>
        </div>
    )
}
