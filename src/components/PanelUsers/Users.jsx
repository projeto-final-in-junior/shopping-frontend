import React, { useState, useEffect } from 'react'
import User from './User'
import * as api from "../../services/apiService";

export default function Users() {
    const [allUsersData, setAllUsersData] = useState([]);

    useEffect( () => {
        api.get_users(setAllUsersData);
    }, [])


    return (
        <div>
            <div id="users_cabecalho">
                <div id="users_cabecalho_internal">
                    <p className="user_cabecalho">Nome</p>
                    <p className="user_cabecalho">Email</p>
                    <p className="user_cabecalho">CPF/CNPJ</p>
                </div>
          
            
            </div>
            <div>
            {allUsersData.map( user => {
                return <User user={user} />
            })}
            </div>
        </div>
    )
}
