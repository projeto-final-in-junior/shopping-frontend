import React from 'react'
import './PanelUsers.css'
import Users from './Users'

export default function PanelUsers() {
    return (
        <div id="div-users">
            <div id="panel_princ_users">
                <div><h3>Usuários</h3></div>
                <div>
                    <Users />
                </div>
            </div>
        </div>
    )
}
