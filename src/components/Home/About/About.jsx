import React, { useState, useEffect } from 'react'
import * as api from '../../../services/apiService'

export default props => {
    const [aboutTxt, setAboutTxt] = useState("")
    
    useEffect(() => {
        api.get_aboutus(setAboutTxt)
      }, []);

    return (
        <div className="about">
            <h2>Sobre nós</h2>

            <div>
                {aboutTxt}
                <img src={require('../../../img/team.jpg')}alt=""/>
            </div>
        </div>
    )
}