import React from 'react'

import Landing from './Landing/Landing'
import BestSeller from './BestSeller/BestSeller'
import About from './About/About'

import './Home.css'

export default props => {

    return (
        <div>
            <Landing />
            <BestSeller />
            <About />
        </div>
    )
}