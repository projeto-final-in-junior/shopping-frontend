import React from 'react'
import ProductsContainer from './ProductsContainer'

export default props => {

    return (
        <div className="best-seller">
            <h2>Nossos produtos mais curtidos</h2>
            <ProductsContainer />
        </div>
    )
}