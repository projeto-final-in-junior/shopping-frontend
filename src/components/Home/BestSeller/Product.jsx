import React, {useState, useEffect} from 'react'
import { Link } from 'react-router-dom'

export default props => {
    const [image, setImage] = useState('')

    useEffect( () => {
        setImage(props.product.images[0])
    }, [props.product])

    const getImage = () => {
        if(image) {
            const url = image.picture.url
            return url
        }
    }
    return (
        <Link className="best-seller-product" to={"/products/" + props.product.id}>
            <img ssrc={`https://warm-escarpment-07318.herokuapp.com${getImage()}`} alt="Foto de um produto"/>

            <h2>{props.product.name.length > 15 ? props.product.name.slice(0, 15) + "..." : props.product.name}</h2>
            <p>{props.product.like_amount} likes</p>
            {/* <p>{props.product.category}</p> */}
            <h5>R$ {props.product.price.toFixed(2)}</h5>
        </Link>
    )
}