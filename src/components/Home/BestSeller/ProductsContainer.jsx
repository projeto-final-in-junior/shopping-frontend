import React, { useState, useEffect} from 'react'
import * as api from '../../../services/apiService'

import Product from './Product'

export default props => {
    const [products, setProducts] = useState([])
    
    useEffect( () => {
        api.get_top_3_products(setProducts)
    }, [])

    return (
        <div className="products-container">
            {products.map( product => {
                return <Product product={product}/>
            })}
        </div>
    )
}