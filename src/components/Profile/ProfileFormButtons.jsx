import React from 'react'

export default props => {

    return (
        <div className="profile-form-buttons">
            <button className="btn-preset">Cancelar</button> 
            <input className="btn-preset"type="submit" value="Confirmar" onClick={props.handleSubmit}/>
        </div>
    )
}