import React, { useState, useEffect } from "react";
import * as api from "../../services/apiService";

import ProfileFormButtons from "./ProfileFormButtons";

import "./ProfileForm.css";

export default (props) => {
  const [name, setName] = useState("");
  const [secondName, setSecondName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");
  //const [cep, setCep] = useState("");
  //const [distric, setDistrict] = useState("");
  //const [city, setCity] = useState("");
  //const [state, setState] = useState("");
  //const [street, setStreet] = useState("");
  //const [extra, setExtra] = useState("");
  //const [number, setNumber] = useState("");
  const [cellphoneNumber, setCellphoneNumber] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const handleInputChange = (e) => {
    const value = e.target.value;

    // Verifica qual input foi modificado e altera o valor do estado equivalente
    switch (e.target.name) {
      case "name":
        setName(value);
        break;
      case "cep":
        //setCep(value);
        break;
      case "district":
        //setDistrict(value);
        break;
      case "city":
        //setCity(value);
        break;
      case "state":
        //setState(value);
        break;
      case "street":
        //setStreet(value);
        break;
      case "extra":
        //setExtra(value);
        break;
      case "number":
        //setNumber(value);
        break;
      case "secondName":
        setSecondName(value);
        break;
      case "email":
        setEmail(value);
        break;
      case "password":
        setPassword(value);
        break;
      case "password_confirmation":
        setPasswordConfirmation(value);
        break;
      case "phone_number":
        setPhoneNumber(value);
        break;
      case "cellphone_number":
        setCellphoneNumber(value);
        break;
      default:
    }
  };

  const cancelOperation = () => {
    window.reload();
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    api.edit_profile(name, secondName, email, password, passwordConfirmation, cellphoneNumber, phoneNumber);

  };

  useEffect(() => {
    const profileInfo = async () => {
      const profileData = await api.get_profile();
      setName(profileData.name)
      setSecondName(profileData.second_name)
      setEmail(profileData.email)
      setCellphoneNumber(profileData.cellphone_number)
      setPhoneNumber(profileData.phone_number)
    };
    profileInfo();
  }, []);

  return (
    <form className="profile-form">
      <div className="profile-personal-form">
        <input
          type="text"
          name="name"
          placeholder="Nome"
          value={name}
          onChange={handleInputChange}
        />
        <input
          type="text"
          name="secondName"
          placeholder="Segundo nome"
          value={secondName}
          onChange={handleInputChange}
        />
        <input
          type="email"
          name="email"
          placeholder="Email"
          value={email}
          onChange={handleInputChange}
        />
        <input
          type="password"
          name="password"
          placeholder="Senha"
          onChange={handleInputChange}
        />
        <input
          type="password"
          name="password_confirmation"
          placeholder="Confirmar senha"
          onChange={handleInputChange}
        />
        <input
          type="number"
          name="cellphone_number"
          placeholder="Telefone celular"
          value={cellphoneNumber}
          onChange={handleInputChange}
        />
        <input
          type="number"
          name="phone_number"
          placeholder="Telefone residencial"
          value={phoneNumber}
          onChange={handleInputChange}
        />
      </div>

      {/* <div className="profile-address-form">
                <h2>Endereços</h2>
                <input type="text" name="address_name" placeholder="Como gostaria de salvar o endereço"/>
                <input type="text" name="receiver" placeholder="Nome completo de quem vai receber"/>
                <input type="text" name="cep" placeholder="CEP" onChange={handleInputChange}/>
                <input type="text" name="street" placeholder="Endereço" onChange={handleInputChange}/>
                <input type="text" name="extra" placeholder="Complemento" onChange={handleInputChange}/>
                <div>
                    <input type="text" name="number" placeholder="Número" onChange={handleInputChange}/>
                    <input type="text" name="district" placeholder="Bairro" onChange={handleInputChange}/>
                </div>
                <div>
                    <input type="text" name="city" placeholder="Cidade" onChange={handleInputChange}/>
                    <input type="text" name="state" placeholder="UF" onChange={handleInputChange}/>
                </div>
            </div> */}

      <ProfileFormButtons
        cancelOperation={cancelOperation}
        handleSubmit={handleSubmit}
      />
    </form>
  );
};
