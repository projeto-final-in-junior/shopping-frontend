import React from 'react'
import ProfileForm from './ProfileForm'

import './Profile.css'

export default props => {

    return (
        <div className="profile">
            <h2>Editar perfil</h2>
            <ProfileForm />
        </div>
    )
}