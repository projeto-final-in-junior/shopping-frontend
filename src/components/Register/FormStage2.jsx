import React from 'react'

export default props => {

    return (
        <div className="register-stage-2">
            <input type="text" name="cep" placeholder="CEP" onChange={props.handleInputChange}/>
            <input type="text" name="street" placeholder="Endereço" onChange={props.handleInputChange}/>
            <input type="text" name="extra" placeholder="Complemento" onChange={props.handleInputChange}/>
            <div>
                <input type="text" name="number" placeholder="Número" onChange={props.handleInputChange}/>
                <input type="text" name="district" placeholder="Bairro" onChange={props.handleInputChange}/>
            </div>
            <div>
                <input type="text" name="city" placeholder="Cidade" onChange={props.handleInputChange}/>
                <input type="text" name="state" placeholder="UF" onChange={props.handleInputChange}/>
            </div>
            <button className="btn-preset register-continue-button" onClick={props.nextPage}>Continuar</button>
        </div>
    )
}