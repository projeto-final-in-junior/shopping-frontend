import React from 'react'

export default props => {

    return (
        <div>
            <input type="text" name="cellphone_number" placeholder="Telefone celular" onChange={props.handleInputChange}/>
            <input type="text" name="phone_number" placeholder="Telefone residencial" onChange={props.handleInputChange}/>
            <input className="btn-preset register-continue-button" type="submit" placeholder="Logar" onClick={props.onSubmit}/>
        </div>
    )
}