import React, { useState, useEffect } from "react";
import * as api from '../../services/apiService';

import FormStage1 from "./FormStage1";
import FormStage2 from "./FormStage2";
import FormStage3 from "./FormStage3";

import "./RegisterForm.css";

export default (props) => {
  const [stage, setStage] = useState(1);
  const [formState, setFormState] = useState({
    "user" : {
      "name": "",
      "second_name": "",
      "email": "",
      "password": "",
      "password_confirmation": "",
      "cep": "",
      "district": "",
      "city": "",
      "state": "",
      "street": "",
      "extra": "",
      "number": "",
      "phone_number": "",
      "cellphone_number": ""
    }
  })

  const handleInputChange = (e) => {
    e.persist()
    const value = e.target.value;
    
    setFormState( () => {
      let next = formState
      next.user[e.target.name] = value
      return next
    })
  };

  const handleUserSubmit = async (event) => {
    event.preventDefault();
    
    await api.sign_up(formState)
    
  };

  useEffect(() => {
    setStage(1);
  }, [props.resetForm]);

  const resetStage = () => {
    setStage(1);
  };

  const nextPage = () => {
    setStage(stage + 1);
  };

  return (
    <form className="register-form" onSubmit={handleUserSubmit}>
      
      {stage === 1 ? (
        <FormStage1 handleInputChange={handleInputChange} nextPage={nextPage} />
      ) : null}

      {stage === 2 ? (
        <FormStage2 handleInputChange={handleInputChange} nextPage={nextPage} />
      ) : null}

      {stage === 3 ? (
        <FormStage3 handleInputChange={handleInputChange}resetStage={resetStage}
        />
      ) : null}

    </form>
  );
};
