import React, { useState } from 'react'

import RegisterForm from './RegisterForm'
import './RegisterModal.css'

export default props => {
    const toggleClass = props.canShow ? "login-modal display-block" : "login-modal display-none";
    const [resetForm, setResetForm] = useState(0)

    const handleClose = () => {
        props.handleClose()
        setResetForm(resetForm + 1)
    }

    return (
        <div className={toggleClass}>
            <section className="register-modal-main">
                <div>

                    <h2>Fazer cadastro</h2>
                    <button className="btn-preset" onClick={handleClose}>X</button>
                </div>
                <RegisterForm resetForm={resetForm}/>
            </section>
        </div>
    )
}