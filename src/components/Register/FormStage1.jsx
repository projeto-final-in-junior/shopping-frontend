import React from 'react'

export default props => {

    return (
        <div className="register-input-container">
            <input type="text" name="name" placeholder="Nome" onChange={props.handleInputChange}/>
            <input type="text" name="second_name" placeholder="Sobrenome" onChange={props.handleInputChange}/>
            <input type="email" name="email" placeholder="Email" onChange={props.handleInputChange}/>
            <input type="password" name="password" placeholder="Senha" onChange={props.handleInputChange}/>
            <input type="password" name="password_confirmation" placeholder="Confirmar senha" onChange={props.handleInputChange}/>
            <button className="btn-preset register-continue-button" onClick={props.nextPage}>Continuar</button>
        </div>
    )
}