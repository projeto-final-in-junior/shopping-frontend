import React from 'react'

export default function User(props) {
    
    
  const handleAddStock = () => {
    props.handleToggleModal(true, props.product.id, props.product.name, props.product.quantity);
}
    return (
        <div id="stock_card">
            <p className="stock_card_item">{props.product.name}</p>
            <p className="stock_card_item">{props.product.quantity}</p>
            <button className="stock_card_item btn-preset" onClick={handleAddStock}>Adicionar</button>
        </div>
    )
}
