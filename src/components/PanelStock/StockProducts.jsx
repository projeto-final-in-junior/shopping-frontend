import React, { useState, useEffect } from 'react'
import StockProduct from './StockProduct'
import * as api from "../../services/apiService";

export default function Users(props) {
    const [allProductsData, setAllProductsData] = useState([]);
    const [toggleModal, setToggleModal] = useState(false);
    const [productNameRestock, setProductNameRestock] = useState("");
    const [productQtdRestock, setProductQtdRestock] = useState("");
    const [productIdRestock, setProductIdRestock] = useState("");
    const [newQtdToRestock, setNewQtdToRestock ] = useState("");

    const handleToggleModal = (param, id, name, qtd) => {
        setToggleModal(param);
        setProductNameRestock(name);
        setProductQtdRestock(qtd);
        setProductIdRestock(id);
    }

    const handleInputChange = (e) => {
        setNewQtdToRestock(e.target.value);
    }

        
    const handleAddToStock = async () => {
        const newQtd = parseInt(productQtdRestock) + parseInt(newQtdToRestock);
        await api.add_product_stock(productIdRestock, newQtd);
        window.location.reload();
    }

    useEffect( () => {
        api.get_products(setAllProductsData);
    }, [])


    return (
        <div>
            <div>
            {allProductsData.map( product => {
                return <StockProduct handleToggleModal={handleToggleModal} product={product} />
            })}
            </div>
            <div class="div_de_fora_do_modal">{toggleModal && <div id="AddStockModal">
                <p>Adicionando "{productNameRestock}"</p>
                <input onChange={handleInputChange}></input>
                <button className="stock_card_item btn-preset" onClick={handleAddToStock}>Adicionar</button>
                </div>}</div>
        </div>
    )
}
