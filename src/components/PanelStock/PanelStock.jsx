import React from 'react'
import './PanelStock.css'
import StockProducts from './StockProducts'

export default function PanelStock() {
    return (
        <div id="div-stock">
            <div id="panel_princ_stock">
                <div><h3>Estoque</h3></div>
                <div>
                    <StockProducts />
                </div>
            </div>
        </div>
    )
}
