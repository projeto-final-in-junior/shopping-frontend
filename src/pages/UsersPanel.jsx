import React from 'react'
import Header from '../components/Header/Header.jsx'
import Footer from '../components/Footer/Footer.jsx'
import SideBar from '../components/Dashboard/SideBar.jsx'
import PanelUsers from '../components/PanelUsers/PanelUsers.jsx'

export default props => {

    return (
        <div>
            <Header />
            <SideBar> 
                <PanelUsers></PanelUsers>
            </SideBar>
            <Footer />
        </div>
    )
}