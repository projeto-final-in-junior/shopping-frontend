import React from 'react'

import HomeComponent from '../components/Home/Home.jsx'
import Header from '../components/Header/Header.jsx'
import Footer from '../components/Footer/Footer.jsx'

export default function Home() {
  return (
    <div>
      <Header />
      <HomeComponent />
      <Footer />
    </div>
  );
}
