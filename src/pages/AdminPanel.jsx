import React from 'react'
import Header from '../components/Header/Header.jsx'
import Footer from '../components/Footer/Footer.jsx'
import SideBar from '../components/Dashboard/SideBar.jsx'
import PanelAdmin from '../components/PanelAdmin/PanelAdmin.jsx'

export default props => {

    return (
        <div>
            <Header />
            <SideBar> 
                <PanelAdmin></PanelAdmin>
            </SideBar>
            <Footer />
        </div>
    )
}