import React from 'react'
import Header from '../components/Header/Header.jsx'
import Footer from '../components/Footer/Footer.jsx'
import SideBar from '../components/Dashboard/SideBar.jsx'

export default function Dashboard() {
    return (
        <div>
            <Header />
            <SideBar />
            <Footer />
        </div>
    )
}
