import React from 'react'
import Header from '../components/Header/Header.jsx'
import Footer from '../components/Footer/Footer.jsx'
import './not_found.css'

export default function NotFound() {
    return (
        <div>
            <Header />
            <section class = "not-found-section"><figure class= "not-found-figure">
            <img class= "not-found-image" src = "https://images.pexels.com/photos/4321260/pexels-photo-4321260.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260" ></img></figure>
            <p id="numeros">404</p>
            <p>Seu lobo favorito não foi achado</p>
            </section>
            <Footer />
        </div>
    )
}
