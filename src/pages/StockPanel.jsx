import React from 'react'
import Header from '../components/Header/Header.jsx'
import Footer from '../components/Footer/Footer.jsx'
import SideBar from '../components/Dashboard/SideBar.jsx'
import PanelStock from '../components/PanelStock/PanelStock.jsx'

export default props => {

    return (
        <div>
            <Header />
            <SideBar> 
                <PanelStock></PanelStock>
            </SideBar>
            <Footer />
        </div>
    )
}