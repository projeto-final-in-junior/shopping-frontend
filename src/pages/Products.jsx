import React from 'react'

import Products from '../components/Products/Products.jsx'
import Header from '../components/Header/Header.jsx'
import Footer from '../components/Footer/Footer.jsx'

export default props => {
  return (
    <div>
      <Header />
      <Products />
      <Footer />
    </div>
  );
}
