import React from "react";
import Header from "../components/Header/Header.jsx";
import Footer from "../components/Footer/Footer.jsx";
import Kart from "../components/Cart/Cart.jsx";

export default function Shoppingcart() {
  return (
    <div>
      <Header />
        <Kart />
      <Footer />
    </div>
  );
}
