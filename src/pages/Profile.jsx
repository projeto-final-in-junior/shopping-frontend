import React from 'react'
import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'

import Profile from '../components/Profile/Profile'

export default props => {

    return (
        <div className="profile">
            <Header />
            <Profile />
            <Footer />
        </div>
    )
}