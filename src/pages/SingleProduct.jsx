import React from 'react'
import Footer from '../components/Footer/Footer.jsx'
import Product from '../components/SingleProduct/Product.jsx';
import Header from '../components/Header/Header.jsx';

export default function Products() {

  
  return (
    <div>
      <Header />
      <Product />
      <Footer />
    </div>
  );
}
