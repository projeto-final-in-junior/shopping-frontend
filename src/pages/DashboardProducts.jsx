import React from 'react'
import Header from '../components/Header/Header.jsx'
import Footer from '../components/Footer/Footer.jsx'
import SideBar from '../components/Dashboard/SideBar.jsx'
import PanelProducts from '../components/PanelProducts/PanelProducts.jsx'

export default function DashboardProducts() {
    return (
        <div>
            <Header />
            <SideBar />
            <PanelProducts />
            <Footer />
        </div>
    )
}
