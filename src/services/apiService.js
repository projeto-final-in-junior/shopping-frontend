import axios from "axios";

const api = axios.create({
  baseURL: "https://warm-escarpment-07318.herokuapp.com/",
});

const user = JSON.parse(localStorage.getItem('user'));
async function sign_up(formState) {
  
  await api.post("/register", formState)
    .then( response => {
      window.alert("Cadastro realizado com sucesso")
    }).catch ( error => {
      window.alert("Não foi possível realizar o cadastro. Verifique suas informações e tente novamente")
    })
  
};

async function login(formState) {
  await api.post("/login", formState)
    .then( response => {
      localStorage.setItem('user', JSON.stringify(response.data))
      check_admin()
    }).catch ( error => {
      window.alert("Não foi possível fazer o login. Verifique suas informações e tente novamente")
    })
}

async function get_profile() {
  const axiosConfig = {
        headers: {
          "Content-Type": "application/json",   
          "Authorization": user.token
        },
      };
  try {
    const res = await api.get("/profile", axiosConfig);
    const resData = res.data;
    return resData;
    
  } catch (error) {
  }
}

async function edit_profile(name, secondName, email, password, passwordConfirmation, cellphoneNumber, phoneNumber) {
  const axiosConfig = {
        headers: {
          "Content-Type": "application/json",   
          "Authorization": user.token
        },
      };
      try {
        const res = await api.put("/profile/edit",
        {
          "user":{
            "name":name,
            "second_name":secondName,
            "email":email,
            "password":password,
            "password_confirmation":passwordConfirmation,
            "phone_number":phoneNumber,
            "cellphone_number":cellphoneNumber,
          }                                                                        
        }
        , axiosConfig);   
        const resData = res.data;
        window.alert("Dados alterados com sucesso!")
        return resData;
      } catch (error) {
        window.alert("Não foi possível alterar os dados cadastrados. Verifique suas informações e tente novamente" + error)
      }
    }

// Função que retorna todos os produtos no db
async function get_products(setProducts) {
  
  await api.get("/products")
    .then (response => {
      setProducts(response.data)
    }).catch (error => {
      alert("Não foi possível resgatar os produtos")
    })

}

async function get_single_product(productId, setProduct, setProductCategory, setProductComments) {

  await api.get('/products/' + productId)
    .then (response => {
      setProduct(response.data)
      setProductCategory(response.data.category)
      setProductComments(response.data.comments)
    })
}

async function get_top_3_products(setProducts) {
    
  await api.get("/top_3_products")
    .then (response => {
      setProducts(response.data)
    }).catch (error => {
      alert("Não foi possível resgatar os produtos")
    })

}

async function post_comment(productId, comment) {
  if (user) {
    let commentBody = {
      "comment":{
        "product_id":productId,
        "content":comment,
      }
    }
  
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };
  
    await api.post("/comment", commentBody, axiosConfig)
      .then ( response => {
        window.location.reload()
      }).catch ( error => {
      })
  } else {
    // alert("Logue para realizar esta ação")
  }
}

async function like_product(productId, setLiked, incLikes) {
  if (user) {
    const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
      },
    };
  
    await api.post('/product/like', { "like": {"product_id": productId}}, axiosConfig)
      .then( response => {
        setLiked(true)
        incLikes()
      })
  } else {
    // alert("Logue para realizar esta ação")
  }
}

async function unlike_product(productId, setLiked, decLikes) {
  if (user) {
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };
  
    await api.post('/product/like/remove', { "like": {"product_id": productId}}, axiosConfig)
      .then( response => {
        setLiked(false)
        decLikes()
      }).catch (error => {
      })
  } else {
    // alert("Logue para realizar esta ação")
  }
}

async function like_comment(commentId, setLiked, updateLikes) {
  if (user) {
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };
  
    await api.post('/comment/like', { "like": {"comment_id": commentId}}, axiosConfig)
      .then( response => {
        setLiked(true)
        updateLikes()
      })
  } else {
    // alert("Logue para realizar esta ação")
  }
}

async function unlike_comment(commentId, setLiked, decLikes) {
  if (user) {
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };
  
    await api.post('/comment/like/remove', { "like": {"comment_id": commentId}}, axiosConfig)
      .then( response => {
        setLiked(false)
        decLikes()
      })
  } else {
    // alert("Logue para realizar esta ação")
  }
}

async function check_comment_like(product_id, setLikedList) {
  if (user) {
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };
  
    const postBody = { "log": {
      "product_id":  product_id
    }}
  
    api.post('/comments/like/log', postBody, axiosConfig)
      .then (response => {
        setLikedList(response.data)
      })
  } else {
    // alert("Logue para realizar esta ação")
  }
}

async function check_product_like(productId, setLiked) {
  if (user) {
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };
  
    const postBody = { "log": {
      "product_id":  productId 
    }}
  
    api.post('/product/like/log', postBody, axiosConfig)
      .then (response => {
        if (response.data) {
          setLiked(true)
        }
      })
  } else {
    // alert("Logue para realizar esta ação")
  }
}

async function get_categories(setCategories) {
  await api.get('/category')
    .then( response => {
      setCategories(response.data)
    })
}

async function get_category(setCategoryData) {
  await api.get("/category")
    .then( response => {
      setCategoryData(response.data);
    }).catch ( error => {
      alert("Não foi possível consultar as categorias. Verifique e tente novamente" + error)
    })
}

async function edit_category(param) {
  await api.put("/category/edit/" + param.id)
    .then( response => {
      alert("Editado!")
    }).catch ( error => {
      alert("Não foi possível consultar as categorias. Verifique e tente novamente" + error)
    })
}

async function add_category(name) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };
  
  if (name !== "") {
    await api.post("/category/add",
    {
      "category":{
        "name": name
        }
      }, axiosConfig)
      .then( response => {
        alert("Categoria adicionada!")
      }).catch ( error => {
        alert("Não foi possível adicionar a categoria. Verifique e tente novamente" + error)
      })
  }
}

async function delete_category(id) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };

  await api.delete(`/category/remove/${id}`, axiosConfig)
    .then( response => {
      alert("Categoria excluida!")
    }).catch ( error => {
      alert("Não foi possível deletar. Verifique se não há nenhum produto cadastrado nesta categoria.")
    })
}

async function delete_product(id) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };

  await api.delete(`/product/remove/${id}`, axiosConfig)
    .then( response => {
      alert("Produto excluído")
      window.location.reload()
    }).catch ( error => {
      alert("Não foi possível deletar o produto")
    })
}

async function get_users(setAllUsersData) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };
  await api.get("/users", axiosConfig)
    .then( response => {
      setAllUsersData(response.data);
    }).catch ( error => {
      alert("Não foi possível consultar os usuários. Verifique e tente novamente" + error)
    })
}

async function delete_user(id) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };
  await api.delete(`/users/${id}`, axiosConfig)
    .then( response => {
      alert("Usuário deletado com sucesso!")
    }).catch ( error => {
      alert("Não foi possível deletar o usuário." + error)
    })
}

async function changeUserPrivileges(name) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };
  
    await api.delete(`/admin/${name}`, axiosConfig)
      .then( response => {
        alert("Privilégios do usuário alterados com sucesso!");
      }).catch ( error => {
        alert("Não foi possível alterar os privilégios do usuários. Verifique e tente novamente");
      })
}



async function add_product_stock(id, newQtd) {
  const axiosConfig = {
        headers: {
          "Content-Type": "application/json",   
          "Authorization": user.token
        },
      };
      try {
        const res = await api.put(`/products/${id}`,
        {
          "product":{
          "quantity": newQtd
            }
          }
        , axiosConfig);   
        const resData = res.data;
        window.alert("Dados alterados com sucesso!")
        return resData;
      } catch (error) {
        window.alert("Não foi possível alterar os dados cadastrados. Verifique suas informações e tente novamente" + error)
      }
    }

async function add_image(image, id) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };

  var data = new FormData()
  data.append("picture", image)
  data.append("product_id", id)

  api.post("/images", data, axiosConfig)
    .then(res => {
    })
}

async function create_product(product, images) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };

  api.post("/products/add", product, axiosConfig)
    .then(response => {
      Array.from(images).map(image => {
        add_image(image, response.data.id)
      })
      alert("Produto criado com sucesso")
      window.location.reload()
    }).catch(error => {
      alert("Não foi possível criar o produto, verifique os dados e tente novamente.")
    })
}

async function check_admin(setIsAdmin) {
  if (user){
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };
  
    api.get("/admin", axiosConfig)
      .then(response => {
        setIsAdmin(true)
        localStorage.setItem("admin", true)
      }).catch(error => {
        localStorage.setItem("admin", false)
      })
  }
}

async function get_aboutus(setAboutTxt) {

    api.get("/texts/aboutus")
      .then(response => {
        setAboutTxt(response.data.content)
      })
};

async function put_aboutus(newAboutTxt) {
  const axiosConfig = {
        headers: {
          "Content-Type": "application/json",   
          "Authorization": user.token
  }}

  api.put("/texts/aboutus",  {
    "text":{
      "section": "aboutus",
      "content": newAboutTxt
       }
    }
    ,axiosConfig)
    .then( response => {
      alert("Texto da Seção AboutUs alterado com sucesso");
    }).catch ( error => {
      alert("Não foi possível o texto. Verifique e tente novamente" + error);
    })
};



async function add_to_cart(productId, quantity) {
  if (user) {
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };

    const postBody = {
      "kart": {
        "product_id":productId,
        "quantity_to_be_bought": quantity
      }
    }

    await api.post("/kart/add", postBody, axiosConfig)
      .then(res => console.log(res))
  }
}

async function get_cart(setCart) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };

  await api.get("/kart", axiosConfig)
    .then(response => {
      setCart(response.data)
    })
}

async function clean_cart(setCart) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    }
  }

  await api.delete("/kart/clear", axiosConfig)
    .then(response => {
      alert("Seu carrinho foi limpo com sucesso")
    })
}

async function edit_quantity_on_cart(productId, quantity) {
  if (user) {
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",   
        "Authorization": user.token
      },
    };

    const postBody = {
      "kart": {
        "product_id": productId,
        "quantity_to_be_bought": quantity
      }
    }

    await api.put("/kart/edit", postBody, axiosConfig)

  }
}

async function delete_product_from_cart(productId) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };

  const postBody = {
    "kart": {
      "product_id": productId,
    }
  }

  await api.post("/kart/remove", postBody, axiosConfig)
    .then (response => {
      window.location.reload()
    })
}

async function confirm_buy(setBuyStatus) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };

  api.delete("/kart/confirm", axiosConfig)
   .then(response => {
      setBuyStatus(true)
    }).catch(error => {
      alert("Não foi possível efetuar a compra")
    })
}

export { 
  login,
  sign_up,
  edit_profile,
  get_profile,
  get_single_product,
  get_category,
  edit_category,
  add_category,
  delete_category,
  get_users,
  delete_user,
  changeUserPrivileges,
  get_products,
  add_product_stock,
  get_top_3_products,
  post_comment,
  like_product,
  like_comment,
  check_comment_like,
  check_product_like,
  unlike_product,
  unlike_comment,
  get_categories,
  delete_product,
  add_image,
  create_product,
  check_admin,
  get_aboutus,
  put_aboutus,
  add_to_cart,
  get_cart,
  clean_cart,
  edit_quantity_on_cart,
  delete_product_from_cart,
  confirm_buy,
}



